<?php

use Illuminate\Support\Facades\Route;
// link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// defined a route wherein a view(form) to create a post will be returned to the user
Route::get('/posts/create', [PostController::class, 'create']);

// Define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts
Route::get('/posts', [PostController::class, 'index']);

// S02 Activity - view posts in random order
Route::get('/', [PostController::class, 'welcome']);

// define a route that will return a view containing only the authenticated users post
Route::get("/myPosts", [PostController::class, 'myPost']);

// define a route that will return a specific posts with matching URL parameter ID({})
Route::get('posts/{id}', [PostController::class, 'show']);

// S03 Activity - Route to edit a post
Route::get('posts/{id}/edit', [PostController::class, 'edit']);

Route::post('/posts/{id}', [PostController::class, 'update']);

// Activity s04
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// Activity s05
Route::post('/posts/{id}/comment', [PostController::class, 'addComment']);
