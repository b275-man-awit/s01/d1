@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{ $post->title }}</h2>
            <p class="card-subtitle text-muted">Author:{{ $post->user->name }}</p>
            <p class="card-subtitle text-muted">Created at: {{ $post->created_at }}</p>
            <p class="card-text">{{ $post->content }}</p>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"
                data-bs-whatever="@mdo">Comment</button>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <form method="POST" action="/posts/{{ $post->id }}/comment">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Leave a Comment</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="message-text" class="col-form-label">Comment:</label>
                                    <textarea class="form-control" id="content" name="content"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Comment</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>
        </div>
    </div>

    <div class="m-3 mt-5">
        <h1>Comments</h1>
        @foreach($post->postComments as $postComment)
    
        <div class="card">
            <div class="card-body">
                <h2>{{ $postComment->content }}</h2>
                <h6 class="card-text mb-3 text-end">
                    Author: {{$postComment->user->name}}
                </h6>
                <h6 class="card-text mb-3 text-end">
                    Posted On: {{$postComment->created_at}}
                </h6>
          
            </div>
      </div>
      @endforeach

    </div>
    

@endsection
