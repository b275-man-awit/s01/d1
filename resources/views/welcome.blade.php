@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <img class="center-block img-fluid w-75" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" />
            </div>
        </div>
    </div>

    @if(count($posts) > 0)
        @foreach($posts->shuffle() as $post)
            <div class="card text-center my-2">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}">
                            {{$post->title}}
                        </a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                   
                </div>

            </div>
            
        @endforeach
   
    @endif
@endsection