<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
// to access with the authenticated user
use Illuminate\Support\Facades\Auth;
// To have access with queries related to the Post Entity/Model.
use App\Models\Post;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing a form for post creation
    public function create(){
        return view('posts.create');
    } 

    // action to receive the form data and subsequently store said data in the post table
    public function store(Request $request){
        // checks if there is a =n authenticated user
        if(Auth::user()){
            // instantiate a new post object from the Post model
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            // save this post object in the database
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    public function index(){
        $posts = Post::where('isActive', true)->get();
        // The "with()" method will allows us to pass info from the controller to view page
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome(){
        $posts = Post::where('isActive', true)->get();
        return view('welcome')->with('posts', $posts);
    }

    // action to show only the posts authored by the authenticated user.
    public function myPost(){
        if(Auth::user()){
            // We are able to fetch the posts related to a specific user because of the establish relationship
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the url parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::where('id', $id)->with('postComments.user')->first();
        // $postComments = PostComment::where('post_id', $post->id)->get();
        return view('posts.show', ['post' => $post]);
    }

    // action to return a view containing a form for post editting
    public function edit($id){
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);;
    }

    // action to update the post
    public function update(Request $request, $id)
    {
        // Get the post that needs to be updated
        $post = Post::findOrFail($id);

        // Update the post's attributes with the new values from the request
        $post->title = $request->input('title');
        $post->content = $request->input('content');

        // Save the updated post to the database
        $post->save();

        // Redirect the user back to the post's detail page
        return redirect('/posts');
    }

    // // action to remove a post of with the matching URL id parameter.
    // public function destroy($id){
    //     $post = Post::find($id);
        
    //     if(Auth::user()->id == $post->user_id){
    //         $post->delete();
    //     }
    //     return redirect('/posts');
    // }

    public function archive($id){
        $post = Post::find($id);

        $post->isActive = false;
        $post->save();

        return redirect('/posts');
    }

    public function addComment($id, Request $request){
        // $post = Post::find($id);
        $postComment = new PostComment;
        $postComment->content = $request->input('content');
        $postComment->user_id = (Auth::user()->id);
        $postComment->post_id = $id;
        $postComment->save();

        return redirect('/posts/'.$id);
    }
       

}


